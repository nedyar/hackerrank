// factor out common settings
ThisBuild / organization := "org.myproject"
ThisBuild / scalaVersion := "2.12.13"
// set the Scala version used for the project
ThisBuild / version      := "0.1.0-SNAPSHOT"

val scalatestVersion = "3.2.3"
val junitVersion = "4.12"

lazy val root = (project in file("."))
  .settings(
    // set the name of the project
    name := "My Project",

    libraryDependencies ++= Seq(
      "org.scalatest" %% "scalatest" % scalatestVersion % Test,
      "junit" % "junit" % junitVersion % Test
    )
  )