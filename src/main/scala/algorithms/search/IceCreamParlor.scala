package algorithms.search

//import javafx.collections.transformation.SortedList
import scala.annotation.tailrec

object IceCreamParlor {
  
  
  def main(args: Array[String]): Unit = {
//2
//4
//5
//1 4 5 3 2
//4
//4
//2 2 4 3
    val m = 4
    val n = 5
    val flavors = List(1,4,5,3,2)
   // val indexedFlav = (1 to n).zip(flavors) 
   // val sorted = indexedFlav.sortWith((l,r) => l._2 < r._2)
    
    val sortedF = flavors.sortWith(_ > _)
    println("sorted: " + sortedF)
    
//    elaborate(sorted.map(pair => pair._1).toList, m, 2, solution)
//    elaborate(sortedF, m, 1, solution)
    
//    val elabSol = elaborate(sortedF, m, 1, List())
//    println("Elaborate: " + elabSol.reverse)

    val compSol = compute(sortedF, m, 1, List())
    println("Compute: " + compSol.reverse)
        
  }
//    for {
//      e <- sorted
//      
//    }
    
    // should implemented in O(log(n)) using Tree like data structure
  @tailrec
  def findNearest(sortedList: List[Int], toFind: Int): List[Int] = {
    if (sortedList.isEmpty) List()
    else if (sortedList.head <= toFind) sortedList
    else findNearest(sortedList.tail, toFind)
  }
    
    
    
  @tailrec
  def elaborate(sortedList: List[Int], goalSum: Int, deep: Int, solution: List[Int]): List[Int] = {
    val nearest = findNearest(sortedList, goalSum)
    if (deep == 0 && nearest.head == goalSum) solution :+ nearest.head
    else if (deep == 0) solution
    else {        
      elaborate(nearest.tail, goalSum - nearest.head, deep - 1, solution)
    }
  }   
    
  def compute(sortedList: List[Int], goalSum: Int, deep: Int, solution: List[Int]): List[Int] = sortedList match  {
     // XXX da ripristinare
//    case Nil => Nil
//    case x::xs => {
//  	  if(deep == 0 && goalSum - x == 0) x :: solution 
//  	  else if (goalSum - x <= 0) Nil
////    	  else compute(findNearest(xs, goalSum - x), goalSum - x, deep - 1)
//  	  else {
//    	  val innerSol = compute(xs, goalSum - x, deep - 1, solution)
//    	  
//    	  
//    	   
//  		  if (innerSol.isEmpty) {
//  		    if (!xs.isEmpty) {
//  		    	compute(xs.tail, goalSum, deep)      
//  		    }
//  		    else Nil
//  		  }
//  	    
//  	  }
//    }     
    case _ => List(1)
  }
    
//    def subsetSize2(dimension: Int): Stream[(Int, Int)] = {
//      for
//    }
    
//    def fun(m: Int, list: List[(Int, Int)], out: List[Int]): List[Int] = {
//      if (m == 0) out.reverse
//      else if (m < 0) {
//        // TODO
//        fun(m )
//      }
//      else if (list.isEmpty) out // error!!
//      else {
//        fun(m - list.head._2, list.tail, out :: list.head._1)
//      }
//    }
    
    
  
}