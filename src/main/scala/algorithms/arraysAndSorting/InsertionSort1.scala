package algorithms.arraysAndSorting

//import org.scalatest.FunSuite
//import org.scalatest.junit.JUnitRunner
//import org.junit.runner.RunWith
import scala.util.Random
import scala.util.Sorting

object InsertionSort1 {
  def main(args: Array[String]): Unit = {
//    val size = scala.io.StdIn.readLine().toInt
//    val array = scala.io.StdIn.readLine().split(" ").map(e => e.toInt)
//    val size = 0
//    val array: Array[Int] = Array()
    val size = 5
    val array: Array[Int] = Array(2, 4, 4, 8, 8)
    insertionSortAlt(size, array)
  }
  
  def insertionSortAlt(size: Int, array:Array[Int]): Unit = {
    if (size == 0 || size == 1) {
      printArray(array, -1)
    }
    else {
      val value = array(size - 1)
      printArray(recoursiveInsertion(value, array, size - 1, true), -10)
    }
  }
  
  def insertionSortAltReturn(size: Int, array:Array[Int]): Array[Int] = {
    if (size == 0 || size == 1) {
      return array
    }
    else {
      val value = array(size - 1)
      recoursiveInsertion(value, array, size - 1, false)
    }
  } 
  
  
  @scala.annotation.tailrec
  def recoursiveInsertion(value: Int, array: Array[Int], index: Int, printStep: Boolean): Array[Int] = {
    if (index == 0 || array(index - 1) <= value) {
    	array(index) = value
			return array
    }
    else {
      array(index) = array(index - 1)
      printArray(array, index, printStep)
      return recoursiveInsertion(value, array, index - 1, printStep)
    }
  }
  
  @deprecated
  def insertionSort(size: Int, array: Array[Int]): Unit = {
    if (size == 0 || size == 1){
      printArray(array, -1)
    }
    else {
      val last = array(size - 1)
      
      if (last > array(size - 2)){
        printArray(array, size - 1)
      }
      else {
        var updated = false
        for (i <- (size - 1) to 1 by -1) {
          if (array(i) > last && array(i - 1) < last) {
            array(i) = last
            updated = true
          }
          else 
            array(i) = array(i - 1)
         printArray(array, i)
        }   
        if (!updated) {
          array(0) = last
          printArray(array, 0)
        }
      }
    }
  }
  
  def printArray(array: Array[Int], i: Int): Unit = {
    printArray(array, i, true)
  }
  
  def printArray(array: Array[Int], i: Int, printStep: Boolean): Unit = {
    if (printStep)
      println(array.mkString(" ") + " index: " + i)
  }
}

//@RunWith(classOf[JUnitRunner])
//class InsertionSortSuite extends FunSuite {
//  test("insertion test") {
//    val magicNumber = 100
//    val size = Random.nextInt(magicNumber)
//    val maxNum = magicNumber
//
//    for (i <- 0 to magicNumber) {
//
//      val randomArray = Seq.fill(size)(Random.nextInt(maxNum)).toArray
//      Sorting.quickSort(randomArray)
//      val toPass = randomArray :+ Random.nextInt(maxNum)
//      InsertionSort1.printArray(toPass, 0)
//      assert(InsertionSort1.insertionSortAlt(size + 1, toPass) == Sorting.quickSort(toPass))
//    }
//  }
//}