package algorithms.arraysAndSorting

object Intro {

  def main(args: Array[String]): Unit = {
    val value = scala.io.StdIn.readLine()
    val size = scala.io.StdIn.readLine().toInt
    val array = scala.io.StdIn.readLine().split(" ")
    
    println(array.indexWhere(s => s == value))
  }
}