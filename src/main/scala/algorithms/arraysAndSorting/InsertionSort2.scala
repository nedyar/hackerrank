package algorithms.arraysAndSorting

object InsertionSort2 {
  def main(args: Array[String]): Unit = {
    //    val size = scala.io.StdIn.readLine().toInt
//    val array = scala.io.StdIn.readLine().split(" ").map(e => e.toInt)
//    val size = 0
//    val array: Array[Int] = Array()
//    val size = 5
//    val array: Array[Int] = Array(2, 4, 1, 5, 7)
    val size = 5
    val array: Array[Int] = Array(1, 4, 3, 5, 6, 2)
    val list = array.toList
    
    insertionSort2(Array(list.head), list.tail, false)
  }
  
  @scala.annotation.tailrec
  def insertionSort2(sortedArray: Array[Int], remaining: List[Int], print: Boolean): Unit = {
    if (remaining.isEmpty)
      InsertionSort1.printArray(sortedArray, -10)
    else {
      val concatArray = sortedArray ++ remaining.toArray[Int]
      if (print) {
    	  InsertionSort1.printArray(concatArray, sortedArray.size)
      }

    	val inputArr = sortedArray :+ remaining.head
      val size = inputArr.size 
      insertionSort2(InsertionSort1.insertionSortAltReturn(size, inputArr), remaining.tail, true)
    }
  }  
}