package algorithms.strings

import scala.annotation.tailrec

object PalindromeIndex {

  def main(args: Array[String]): Unit = {
   val n = scala.io.StdIn.readLine().toInt  
   for(i <- 0 to n - 1) {
      val str = scala.io.StdIn.readLine()   
     
      if (isPalindrome(str)) 
        println(-1)
      else if (str.length() == 2)
        println(0)
      else 
        println(obtainIndex(str))
    }
  }  
  
  def isPalindrome(str: String): Boolean = {
    return str == str.reverse
  }
  
  def isPalindrome(prefix: List[Char], suffix: List[Char]): Boolean = {
    val composedStr = prefix.mkString + suffix.mkString
    return isPalindrome(composedStr)
  }
  
  def obtainIndex(str: String): Int = {
    
    @tailrec
    def internalRec(alreadyReversed: List[Char], remaining: List[Char], index: Int): Int = {
      if (remaining.isEmpty || remaining.tail.isEmpty)
    	  index
      else if (isPalindrome(alreadyReversed.reverse, remaining.tail)) 
        index
      else 
        internalRec(remaining.head :: alreadyReversed, remaining.tail, index + 1)
    }
    
    internalRec(List(), str.toCharArray().toList, 0)
  }
}