import scala.annotation.tailrec

@tailrec
def strReduction(strList: List[Char], already: Set[Char], acc: StringBuilder): String = strList match {
  case Nil => acc.mkString
  case x :: xs if(already.contains(x)) => strReduction(xs, already, acc)
  case x :: xs if(!already.contains(x)) => strReduction(xs, already + x, acc.append(x))
}

strReduction("aaabbbcdd".toCharArray().toList, Set(), new StringBuilder())