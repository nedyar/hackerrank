package codeGolf

object TheTrigonometricRatios {

 
  
  def main(args: Array[String]): Unit = {
    two()
  }
  
  
  def one(): Unit = {
    val c_list = List(0,2,4,6,8)
    val s_list = List(1,3,5,7,9)
    val x = 2.83
    def fun(l:List[Int]) = List.range(0, l.size).zip(l).map(n => Math.pow(-1, n._1) * Math.pow(x, n._2)/fac(n._2)).foldLeft(0.0)(_ + _)
    def fac(n: Int) = List.range(1, n+1).foldLeft(1)(_*_)
    println (fun(s_list))
    println (fun(c_list))   
  }
  
  def two(): Unit = {
    val x = 2.83
    def fac(n: Int) = List.range(1, n+1).foldLeft(1)(_*_)
    def exp(n: Double): Stream[Double] = (Math.pow(x, n)/fac(n.toInt)) #:: exp(n + 2)
    def s(i: Stream[Double]) = i.foldLeft(0.0)(_ + _)
    println(s(exp(0).take(5)))
  }
  
}