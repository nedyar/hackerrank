import scala.annotation.tailrec

val x = "asdParola"
val y = "asdAltraParola"
val x1 = scala.io.StdIn.readLine()
val y1 = scala.io.StdIn.readLine()

def prefixCompression(x: String, y: String): Unit = {
  @tailrec
  def rec(xList: List[Char], yList: List[Char], prefix: StringBuilder): (String, String, String) = (xList, yList) match {
    case (Nil, _) => (prefix.mkString, xList.mkString, yList.mkString)
    case (_, Nil) => (prefix.mkString, xList.mkString, yList.mkString)
    case (x :: xs, y :: ys) if (x == y) => rec(xs, ys, prefix.append(x))
    case (x :: xs, y :: ys) if (x != y) => (prefix.mkString, xList.mkString, yList.mkString)
  }

  def out = rec(x.toCharArray.toList, y.toCharArray.toList, new StringBuilder())
  println(out._1.length + " " + out._1)
  println(out._2.length + " " + out._2)
  println(out._3.length + " " + out._3)
}

//prefixCompression(x, y)
//prefixCompression("puppy", "puppy")
prefixCompression("kitkat", "kit")

