package functional

import scala.io.StdIn
import scala.util.Try

object PoligonPerimeter extends App {

  lazy val n = StdIn.readLine()
  assert(Try(n.toInt).isSuccess && n.toInt >= 3, s"Invalid input $n")
  val lines = (1 to n.toInt).map(_ => StdIn.readLine())

  println(lines)


}
