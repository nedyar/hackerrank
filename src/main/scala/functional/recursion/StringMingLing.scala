package functional.recursion

import scala.io.StdIn

object StringMingLing {
  
  
  def main(args: Array[String]): Unit = {
    lazy val p = StdIn.readLine()
    lazy val q = StdIn.readLine()   
    
    val zipped = p.zip(q)  
       //println(out)
    val out = zipped.map{
        p => p._1 + "" +  p._2
    }
    println(out.flatten.mkString)        
        
  }
}