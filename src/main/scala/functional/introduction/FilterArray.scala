package functional.introduction

object FilterArray {
  def main(args: Array[String]): Unit = {
    println(f(5, List(1,879,2,13,3,65,2)))
  }
  
  def f(delim:Int,arr:List[Int]):List[Int] = arr.filter(x => x < delim)
}


