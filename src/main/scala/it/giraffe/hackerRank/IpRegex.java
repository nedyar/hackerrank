package it.giraffe.hackerRank;

/**
 * Created by Stefano on 13/03/2016.
 */
public class IpRegex {
    private static class myRegex {

        public final String num = "(([0-1][0-9][0-9])|([2][0-5][0-5])|([0-9][0-9])|([0-9]))";
        public final String pattern = "(" + num + "\\.){3}" + num;



    }


    public static void main(String[] args) {

        System.out.println("255".matches(new myRegex().num));

        System.out.println("123.22.15.11.56".matches(new myRegex().pattern));
    }
}


